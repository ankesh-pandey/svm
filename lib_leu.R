library(e1071)
library(SparseM)
library(LiblineaR)
leufile<-"/media/ankeshp/New Volume/Sem 7/cs678/assignments/ass2/binary datasets/leu"
leu<-read.matrix.csr(leufile)
X<-as.matrix(leu$x)
Y<-as.matrix(leu$y)
trainx<-X
trainy<-Y
leu_test<-read.matrix.csr("/media/ankeshp/New Volume/Sem 7/cs678/assignments/ass2/binary datasets/leu.t")
testx<-as.matrix(leu_test$x)
testy<-as.matrix(leu_test$y)
ptm<-proc.time()
model<-svm(trainx,trainy,scale =T,type = 'C-classification',kernel = 'polynomial',degree=2)
proc.time()-ptm
pred<- fitted(model)
table(pred,trainy) # 100 % accuracy on training set
pred<-predict(model,testx)
table(pred,testy) # 75% accuracy on test set (due to less number of instances)
# 10 fold cross validation
indices<-rep(1:10, c(4,4,4,4,4,4,4,4,3,3))
id=sample(indices)
sum=0
for(i in 1:10)
{
  trainx<- X[id!=i,]
  testx <- X[id==i,]
  trainy<- Y[id!=i,]
  testy <- Y[id==i,]
  model<-svm(trainx,trainy,scale = T,type = 'C-classification',kernel = 'linear')
  pred<-predict(model,testx)
  table(pred,testy)
  cci=sum(testy==pred) / nrow(testx)
  sum= sum+cci
}
acc =sum/10
acc # 95% 10 fold cv accuracy
# tuning for rbf kernel default in libsvm
X<- scale(X, center = TRUE, scale = TRUE)
obj <- tune.svm(X,as.numeric(Y), kernel='polynomial',degree=3, cost = 2^(2:4),tunecontrol = tune.control(sampling = "fix"))
summary(obj)
plot(obj) # almost same error throughout this range
