library(e1071)
library(SparseM)
library(kernlab)
covtypefile<-"/media/ankeshp/New Volume/Sem 7/cs678/assignments/ass2/binary datasets/covtype.libsvm.binary.scale"
covtype<-read.matrix.csr(covtypefile)
X1<-as.matrix(covtype$x)
Y1<-as.matrix(covtype$y)
Y1<-replace(Y1, Y1==2, -1)
############## new iteration from here ##########################
train_idx <- sample(1:nrow(X1),floor(0.001*nrow(X1)),replace=FALSE)
X <- X1[train_idx,] # select all these rows
#X<- scale(X, center = TRUE, scale = TRUE)
Y <- Y1[train_idx,]
Y<- as.matrix(Y)
test_idx <- sample(1:nrow(X),floor(0.0002*nrow(X)),replace=FALSE)
testX <- X1[test_idx,] # select all but these rows
#testX<-scale(testX, center = TRUE, scale = TRUE)
testY  <- Y1[test_idx,]
testY <- as.matrix(testY)
len=dim(X)[1]
#SMO
C=20
tol=0.001
max_passes=10
alpha<-matrix(0,len,1)
b=0
passes=0
stopping1<- function (){
  if((as.numeric(Y[i,])*E_i < (-tol) && alpha[i,]<C)||(as.numeric(Y[i,])*E_i>tol && alpha[i,]>0)){
    return (1)
  }
  else{
    sum=0
    for(i in 1:len){
      if(!(alpha[i,]>=tol && alpha[i,]-C<tol)){
        return (1)
      }
      sum = sum + alpha[i,]*as.numeric(Y[i,])
    }
    if(sum>tol){
      return(1)
    }
    return (0)
  }
}
while(passes<max_passes){
  num_changed_alphas=0
  for(i in 1:len){
    result=b
    for(l in 1:len){
      result = result + alpha[l,]*as.numeric(Y[l,])*(t(X[i,])%*%X[l,])
    }
    E_i = result - as.numeric(Y[i,])
    if(stopping1()){
      j = sample(1:len,1)
      while(j==i){
        j = sample(1:len,1)
      }
      result=b
      for(k in 1:len){
        result = result + alpha[k,]*as.numeric(Y[k,])*(t(X[j,])%*%X[k,])
      }
      E_j = result - as.numeric(Y[j,])
      alpha_i_old=alpha[i,]
      alpha_j_old=alpha[j,]
      if(as.numeric(Y[i,])!=as.numeric(Y[j,])){
        L = max(0,alpha[j,]-alpha[i,])
        H = min(C,C+alpha[j,]-alpha[i,])
      }
      if(as.numeric(Y[i,])==as.numeric(Y[j,])){
        L = max(0,alpha[j,]+alpha[i,]-C)
        H = min(C,alpha[j,]+alpha[i,])
      }
      if(L==H){next}
      eta = 2*(t(X[i,])%*%X[j,])-(t(X[i,])%*%X[i,])-(t(X[j,])%*%X[j,])
      if(eta>=0){next}
      alpha[j,] = alpha[j,] - (as.numeric(Y[j,])*(E_i-E_j)/eta)
      if(alpha[j,]>H){alpha[j,]=H}
      if(alpha[j,]<L){alpha[j,]=L}
      if(abs(alpha[j,]-alpha_j_old)<10^(-5)){next}
      alpha[i,] = alpha[i,] + as.numeric(Y[i,])*as.numeric(Y[j,])*(alpha_j_old - alpha[j,])
      b1 = b-E_i-as.numeric(Y[i,])*(alpha[i,]-alpha_i_old)*(t(X[i,])%*%X[i,])-as.numeric(Y[j,])*(alpha[j,]-alpha_j_old)*(t(X[i,])%*%X[j,])
      b2 = b-E_j-as.numeric(Y[i,])*(alpha[i,]-alpha_i_old)*(t(X[i,])%*%X[j,])-as.numeric(Y[j,])*(alpha[j,]-alpha_j_old)*(t(X[j,])%*%X[j,])
      b=(b1+b2)/2
      if(alpha[i,]>0&&alpha[i,]<C){b=b1}
      if(alpha[j,]>0&&alpha[j,]<C){b=b2}
      num_changed_alphas = num_changed_alphas + 1
    }
  }
  if(num_changed_alphas==0){
    passes = passes + 1
  }
  else{
    passes = 0
  }
}
#testing 
pred<-matrix(0,nrow(X),1)
for(i in 1:nrow(X)){
  result=b
  for(l in 1:nrow(X)){
    result = result + alpha[l,]*as.numeric(Y[l,])*(t(X[i,])%*%X[l,])
  }
  if(result>=0){
    pred[i,]=1
  }
  else{
    pred[i,]=-1
  }
}
acc=sum(pred==as.numeric(Y))/nrow(Y)
acc 
pred<-matrix(0,nrow(testX),1)
for(i in 1:nrow(testX)){
  result=b
  for(l in 1:nrow(testX)){
    result = result + alpha[l,]*as.numeric(Y[l,])*(t(testX[i,])%*%X[l,])
  }
  if(result>=0){
    pred[i,]=1
  }
  else{
    pred[i,]=-1
  }
}
acc=sum(pred==as.numeric(testY))/nrow(testY)
acc 