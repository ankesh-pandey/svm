setwd("/home/ankesh/R")
require("e1071")
x<-read.table(file="spambase.data",header=F,sep=",")
x$V58<-as.factor(x$V58)
model<-svm(V58~.,data=x,type='C')
indices<-rep(1:5, c(9,9,9,9,7))
id=sample(indices)
#Matrix<-data
ListX<- split(x, id)
  sum = 0
  for(i in 1:5)
  {
    train<- x[id!=i,]
    test <- x[id==i,]
    cv_model<-svm(V58~.,data=train)  
    pred <- predict(cv_model, newdata=test[,-58],type="response")
    tbl <- table(test[,58], pred)
    cci=sum(test$V58==pred) / nrow(test)
    sum = sum + cci
  }
  sum = sum/5
  error = 1 - sum
   print(error)
  