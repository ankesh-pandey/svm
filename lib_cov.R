library(e1071)
library(SparseM)
library(LiblineaR)
covtypefile<-"/media/ankeshp/New Volume/Sem 7/cs678/assignments/ass2/binary datasets/covtype.libsvm.binary.scale"
covtype<-read.matrix.csr(covtypefile)
X<-as.matrix(covtype$x)
Y<-as.matrix(covtype$y)
Y<-replace(Y, Y==2, -1)
############## new iteration from here ##########################
train_idx <- sample(1:nrow(X),floor(0.1*nrow(X)),replace=FALSE)
trainx <- X[train_idx,] # select all these rows
trainy <- Y[train_idx,]
test_idx <- sample(1:nrow(X),floor(0.02*nrow(X)),replace=FALSE)
testx  <- X[test_idx,] # select all but these rows
testy  <- Y[test_idx,]
ptm<- proc.time()
model<-svm(trainx,trainy,scale = T,type = 'C-classification',kernel = 'linear')
proc.time()-ptm
pred<- fitted(model)
table(pred,trainy) # 100 % accuracy on training set
acc=sum(as.numeric(trainy)==pred) / nrow(as.matrix(trainy))
acc
pred<-predict(model,testx)
table(pred,testy) # 75% accuracy on test set (due to less number of instances)
acc=sum(as.numeric(testy)==pred) / nrow(as.matrix(testx))
acc
# 10 fold cross validation
block=floor(nrow(trainx)/10)
indices<-rep(1:10, c(block,block,block,block,block,block,block,block,block,block+1))
id=sample(indices)
sum=0
for(i in 1:10)
{
  trainx<- X[id!=i,]
  testx <- X[id==i,]
  trainy<- Y[id!=i,]
  testy <- Y[id==i,]
  model<-svm(trainx,trainy,scale = T,type = 'C-classification',kernel = 'linear')
  pred<-predict(model,testx)
  table(pred,testy)
  cci=sum(testy==pred) / nrow(testx)
  sum= sum+cci
}
acc =sum/10
acc # 95% 10 fold cv accuracy
# tuning for rbf kernel default in libsvm
obj <- tune.svm(X,as.numeric(Y),cost = 2^(1:6),tunecontrol = tune.control(sampling = "fix"))
summary(obj)
plot(obj) # almost same error throughout this range
